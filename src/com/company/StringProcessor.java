package com.company;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringProcessor {

    public String StringRepeat(String s, int n) throws IllegalArgumentException {
        String str="";
        if (n < 0) {
            throw new IllegalArgumentException("n<0");
        }
        for (int i = 0; i < n; i++) {
            str+=s;
        }
        return str;
    }

    public int StringEntry(String s, String p) {
        Pattern pattern = Pattern.compile(s,Pattern.LITERAL);
        Matcher matcher = pattern.matcher(p);
        int i = 0;
        while (matcher.find()) {
            i++;
        }
        return i;
    }

    public String StringChanger(String s) {
        StringBuilder string = new StringBuilder();
        int i = 0;
        while (i < s.length()) {
            if (s.charAt(i) == '1') {
                string.append("один");
            } else if (s.charAt(i) == '2') {
                string.append("два");
            } else if (s.charAt(i) == '3') {
                string.append("три");
            } else {
                string.append(s.charAt(i));
            }
            i++;
        }
        return string.toString();
    }

    public StringBuilder StringDeleter(StringBuilder str) throws StringProcessorExceptions
    {
        for(int i=1;i<str.length();i=i+1)
        {
            str.deleteCharAt(i);
        }
        return str;
    }
    public StringBuilder StringSwap(StringBuilder str) throws StringProcessorExceptions {
        if (str.length()==0)
        {
            throw new StringProcessorExceptions("string is empty");
        }
        int length =str.length();
        while (str.charAt(0) == ' ') {
            str.deleteCharAt(0);
            length-=1;
        }
        while (str.charAt(length-1) == ' ') {
            str.deleteCharAt(length-1);
            length-=1;
        }
        int i = 0;
        StringBuilder str1 = new StringBuilder();
        while (str.charAt(i) != ' ') {
            str1.append(str.charAt(i));
            str.deleteCharAt(i);
        }
        StringBuilder str2 = new StringBuilder();
        i = str.length()-1;
        int o = 0;
        while (str.charAt(i) != ' ')
        {
            str2.append(str.charAt(i));
            str.deleteCharAt(i);
            i--;
            o++;
        }
        str2.reverse();
        str.append(str1);
        str2.append(str);
        return str2;
    }
    public String hexToDec(String string) throws StringProcessorExceptions {
        if (string.length() == 0) {
            throw new StringProcessorExceptions("String is empty");
        }
        StringBuilder res = new StringBuilder(string);
        String str;
        int x;
        Pattern hex = Pattern.compile("0x\\d{8}");
        Matcher finder = hex.matcher(res);
        while (finder.find()) {
            x = Integer.parseInt(res.substring(finder.start() + 2, finder.end()), 16);
            str = Integer.toString(x);
            res.replace(finder.start(), finder.end(), str);
            finder = hex.matcher(res);
        }
        return res.toString();
    }
}


