package com.company;

public class StringProcessorExceptions extends Throwable {
    public StringProcessorExceptions(final String message) {
        super(message);
    }

    public StringProcessorExceptions(final String message, Throwable ex) {
        super(message, ex);
    }
}
