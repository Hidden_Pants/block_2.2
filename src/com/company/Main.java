package com.company;

public class Main {

    public static void main(String[] args) throws StringProcessorExceptions {
        StringProcessor stringi = new StringProcessor();
        String str1;
        str1=stringi.StringRepeat("stroka",4);
        System.out.println(str1);
        System.out.print("\n");
        stringi.StringEntry("abc","oifd..sdabc.abconfbn");
        System.out.println(stringi.StringEntry("..","oifd...sdabc..abconfbn"));
        System.out.println(stringi.StringChanger("1234"));

        StringBuilder str = new StringBuilder();
        str.append("123456789");
        System.out.println(stringi.StringDeleter(str));
        str.delete(0,str.length());
        str.append(" why so serious ");
        System.out.println(stringi.StringSwap(str));
        System.out.println(stringi.hexToDec("Васе 0x00000010 лет"));
    }

}
